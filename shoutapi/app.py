from flask import Flask
from werkzeug import find_modules
import importlib

def flask_interface(*args, **kwargs):
    application = Flask("shoutapi")

    for module_name in find_modules("shoutapi.views"):
        module = importlib.import_module(module_name)
        blueprint = getattr(module, "endpoint", None)

        if blueprint:
            application.register_blueprint(blueprint)

    return application(*args, **kwargs)

