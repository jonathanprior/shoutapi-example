from flask import Blueprint, request
import requests
import json

endpoint = Blueprint('shout', __name__, template_folder='templates')

@endpoint.route('/shout', methods=['POST'], defaults={'response_format': 'json'})
@endpoint.route('/shout.<response_format>', methods=['POST'])
def api(response_format):
    supported_formats = {
        'json': json
    }

    # JSON is assumed default
    if response_format.lower() not in supported_formats.keys():
        response_format = 'json'
    
    format_module = supported_formats[response_format]

    # Check if text was provided
    text_postdata = request.form.get('text', None)
    response_json = request.get_json() or {}
    text_json = response_json.get('text', None)

    text = text_postdata or text_json

    if not text:
        return format_module.dumps({'error': 'text is not specified', 'description': 'Please provide a non blank string for this field.'}), 400, {'Content-Type': 'application/' + response_format}

    # Send the request to the Shoutcloud service
    try:
        sc_request = requests.post('http://api.shoutcloud.io/V1/SHOUT', json={'INPUT': text})
    except requests.exceptions.RequestException as error:
        # Pass through errors
        return format_module.dumps({'error': 'internal connection error', 'description': str(error)})

    # Pass through HTTP errors from Shoutcloud service
    try:
        sc_request.raise_for_status()
    except requests.exceptions.RequestException as error:
        return format_module.dumps({'error': 'SHOUTCLOUD service error', 'description': str(error)})

    # Validate response
    try:
        sc_response = sc_request.json()
    except ValueError:
        return format_module.dumps({'error': 'SHOUTCLOUD service error', 'description': 'SHOUTCLOUD service returned invalid JSON'})
    
    if not sc_response.get("OUTPUT", None):
        return format_module.dumps({'error': 'SHOUTCLOUD service error', 'description': 'SHOUTCLOUD service response did not include capitalized text'})

    # Return response
    return format_module.dumps({'original_text': text, 'capitalized_text': sc_response['OUTPUT']})