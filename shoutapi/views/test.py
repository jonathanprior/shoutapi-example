from flask import Blueprint, request, render_template
from urllib.parse import urljoin
import markdown
import requests
import os

endpoint = Blueprint('test', __name__, template_folder='templates')

def unspecified_text(urlroot):
    test_status = {
        'description': 'Return error message when text is not specified in HTTP POST request',
        'expected_output': '{"description": "Please provide a non blank string for this field.", "error": "text is not specified"}'
    }

    req = requests.post(urlroot + "shout")
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("error", None) == "text is not specified":
        test_status['result'] = 'success'
        test_status['output'] = req.text
        return test_status 

def garbage_data(urlroot):
    test_status = {
        'description': 'Return error message when garbage data is specified in HTTP POST request',
        'input': ":!:!!!:!!!:::!!",
        'expected_output': '{"description": "Please provide a non blank string for this field.", "error": "text is not specified"}'
    }

    req = requests.post(urlroot + "shout", data=test_status['input'])
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("error", None) == "text is not specified":
        test_status['result'] = 'success'
        test_status['output'] = req.text
        return test_status 

def specified_text_postdata(urlroot):
    test_status = {
        'description': 'Return capitalized text when valid Latin characters are provided over POSTDATA',
        'input': 'aAa0oktext',
        'expected_output': 'AAA0OKTEXT'
    }

    req = requests.post(urlroot + "shout", data={'text': test_status['input']})
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("capitalized_text", None) == test_status['expected_output']:
        test_status['result'] = 'success'
    else:
        test_status['result'] = 'fail'
    
    test_status['output'] = req.text
    return test_status

def specified_text_json(urlroot):
    test_status = {
        'description': 'Return capitalized text when valid Latin characters are provided over JSON',
        'input': 'aAa0oktext',
        'expected_output': 'AAA0OKTEXT'
    }

    req = requests.post(urlroot + "shout", json={'text': test_status['input']})
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("capitalized_text", None) == test_status['expected_output']:
        test_status['result'] = 'success'
    else:
        test_status['result'] = 'fail'
    
    test_status['output'] = req.text
    return test_status

def specified_text_cyrillic(urlroot):
    test_status = {
        'description': 'Return capitalized text when valid Cyrillic characters are provided over JSON',
        'input': "Калининград",
        'expected_output': "КАЛИНИНГРАД"
    }

    req = requests.post(urlroot + "shout", json={'text': test_status['input']})
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("capitalized_text", None) == test_status['expected_output']:
        test_status['result'] = 'success'
    else:
        test_status['result'] = 'fail'
    
    test_status['output'] = req.text
    return test_status

def specified_text_non_letter(urlroot):
    test_status = {
        'description': 'Return identical text when valid non-letter characters are specified',
        'input': "0123012-301-0-0!!",
        'expected_output': "0123012-301-0-0!!"
    }

    req = requests.post(urlroot + "shout", json={'text': test_status['input']})
    try:
        result = req.json()
    except Exception as e:
        test_status['result'] = 'fail'
        test_status['output'] = e
        return test_status 

    if result.get("capitalized_text", None) == test_status['expected_output']:
        test_status['result'] = 'success'
    else:
        test_status['result'] = 'fail'
    
    test_status['output'] = req.text
    return test_status

@endpoint.route('/test')
def test():
    urlroot = request.url_root
    tests = [
        unspecified_text,
        garbage_data,
        specified_text_postdata,
        specified_text_json,
        specified_text_cyrillic,
        specified_text_non_letter
    ]
    results = []

    for test in tests:
        results.append(test(urlroot))

    return render_template("test.html", results=results)