from flask import Blueprint, render_template
import markdown
import os

endpoint = Blueprint('index', __name__, template_folder='templates')

@endpoint.route('/')
def index():
    f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../README.md"), mode="r", encoding="utf-8")
    return render_template("index.html", html=markdown.markdown(f.read()))