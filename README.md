# ShoutAPI

ShoutAPI is a Flask application that repeats everything you say
at it back to you, but *louder*.

Uses [SHOUTCLOUD](http://shoutcloud.io) for capitalization.

Requires Python 3.


## Installing

Create a Python 3 virtualenv.

    mkvirtualenv --python=/path/to/python3

Install the dependencies using pip.

    pip install -r requirements.txt


## Starting the server

cd into the same directory as requirements.txt, and run the
following command.

    PYTHONPATH="." FLASK_APP="shoutapi.app:flask_interface" flask run --with-threads

The server will start, and can be located at <http://localhost:5000>


## Usage

### /

Displays this README as HTML.

### /shout

Accepts only **POST** requests, in POSTDATA and [JSON][json] formats.

* **text**: The text to capitalize.

Returns a JSON output consisting of your original text and
capitalized text, e.g.

    {'original_text': 'The quick brown fox jumped over the lazy dog.', 'capitalized_text': 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG.'}

#### Errors

Errors will be returned with the following fields:

* **error**: The error message
* **description**: A description or explanation of the error

### /shout.&lt;format&gt;

As above, but will output results in the specified format, e.g.

    POST /shout.json HTTP/1.1
    ...

will output the API result in [JSON][json] format.

### /test

Runs the test suite, and outputs results in HTML.

# Testing

Launch the development server as described above, and then
visit <http://localhost:5000/test> to run the test suite.

[json]: https://json.org
